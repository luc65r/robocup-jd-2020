#include <Grove_I2C_Motor_Driver.h>

#define MOTOR_DRIVER 0x0f //Driver des moteurs

//Initialiser les moteurs
void initMoteurs() {
  Motor.begin(MOTOR_DRIVER);
}

//Avancer
void avancer(byte v) {
  Motor.speed(MOTOR1, -v);
  Motor.speed(MOTOR2, v);
}

//Tourner sur lui-même vers la droite
void rotationDroite(byte v) {
  Motor.speed(MOTOR1, -v);
  Motor.speed(MOTOR2, -v);
}

//Tourner sur lui-même vers la gauche
void rotationGauche(byte v) {
  Motor.speed(MOTOR1, v); 
  Motor.speed(MOTOR2, v);
}

//Tourner vers la droite en avançant
void tournerDroite(byte v) {
  Motor.speed(MOTOR1, -v);
  Motor.stop(MOTOR2);
}

//Tourner vers la gauche en avançant
void tournerGauche(byte v) {
  Motor.stop(MOTOR1);
  Motor.speed(MOTOR2, v);
}

//Arrêter les roues
void arret() {
  Motor.stop(MOTOR1);
  Motor.stop(MOTOR2);
}
