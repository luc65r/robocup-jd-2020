#define DLG 2 //Détecteur de ligne gauche
#define DLM 4 //Détecteur de ligne milieu
#define DLD 6 //Détecteur de ligne droit

//Initialise les capteurs infrarouges
void initLigne() {
  pinMode(DLG, INPUT); //Détecteur de ligne gauche
  pinMode(DLM, INPUT); //Détecteur de ligne milieu
  pinMode(DLD, INPUT); //Détecteur de linge droit
}

//Retourne la position de la ligne avec un nombre de 0 à 7
byte positionLigne() {
  int position = 0;
  if (digitalRead(DLG)) position += 1; //Si la ligne est détectée à gauche
  if (digitalRead(DLM)) position += 2; //Si la ligne est détectée au milieu
  if (digitalRead(DLD)) position += 4; //Si la ligne est détectée à droite
  return position;
}

//Suivre la ligne noire
void suivreLigne(byte v) {
  switch (positionLigne()) {
    case 0: //La ligne n'est pas détectée
      //À réfléchir
      arret();
      break;
    case 1: //La ligne est détectée à gauche
      rotationGauche(v);
      break;
    case 2: //La ligne est détectée au milieu
      avancer(v);
      break;
    case 3: //La ligne est détectée à gauche et au milieu
      rotationGauche(v);
      break;
    case 4: //La ligne est détectée à droite
      rotationDroite(v);
      break;
    case 5: //La ligne est détectée à gauche et à droite
      //À réfléchir
      arret();
      break;
    case 6: //La ligne est détectée à droite et au milieu
      rotationDroite(v);
      break;
    case 7: //La ligne est détectée partout
      //À réfléchir
      arret();
      break;
  }
}
