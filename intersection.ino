/*
#include <GroveColorSensor.h>
#include <Registers.h>
#include <Utilities.h>


GroveColorSensor colorSensor;
*/
int tempsRotation = 1000;

//Détecte le carré vert et qui retourne sa position
byte carreVert() {
  /*
   * Si il n'y a pas de carré, la fonction retourne 0
   * Si le carré est à droite, la fonction retourne 1
   * Si le carré est à gauche, la fonction retourne 2
   * Si il y a un carré de chaque côté, la fonction retourne 3
   */
  byte position;
  return position;
}

//Aborder l'intersection en fonction de la position du carré vert
void intersection(byte position) {
  /*
   * Si le carré est à droite (1), tourner à droite
   * Si le carré est à gauche (2), tourner à gauche
   * Si il y a un carré de chaque côté (3), faire demi-tour
   * 
   * En suposant que les capteurs soient placées de tel sorte que lorsque le carré vert est détécté, le centre du robot soit au centre de l'intersection.
   */
    if (carreVert() == 1) {
    //Tourner de 90°
    rotationDroite(vitesse);
    delay(tempsRotation);
    } 
    else if (carreVert() == 2) {
      rotationGauche(vitesse);
      delay(tempsRotation);
    } 
    else if (carreVert() == 3) {
      rotationGauche(vitesse);
      delay(tempsRotation*2);
    }
    else if (carreVert == 0) {
    }
}
