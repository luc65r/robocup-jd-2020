# Inventaire

## Carte
* VMA101 ATmega2560 MEGA
* Mega Shield

## Chassis rouge
* 1 chassis
* 4 roues jaunes
* 4 moteurs jaunes DG02S 48:1

## Chassis noir
* 1 chassis
* 4 roues noires
* 4 moteurs noirs

## Batteries
* 1 chargeur 8.4V 2A
* 2 batteries Li-ion 7.4V 2.2Ah
* 1 support pour 4 piles AA

## Capteurs
* 2 I2C Color Sensor
* 2 ultrasonic sensor
* 1 acceleromètre
* 2 capteurs de proximités (2-15cm)
* 3 capteurs de reflet infrarouge
* 2 capteurs de température infrarouge
* 2 drivers moteurs
* 1 paire d'encodeur magnétique
* 4 capteurs de fin de course

## Moteurs
* 4 micro-moteurs en 6V
* 4 servo-moteurs en 
* 2 micro-servo-moteurs

## Roues
* 4 roues blanches 

## Entrés
* 1 Bouton poussoir
* 1 Bouton on-off


## Sorties
* 1 écran LCD rétroéclairé

## Raccordement
* 2 T connecteurs (mâle, femelle)
* 1 I2C hub
* 5 cables de 20cm en 4 pins buckled
* 4 cables de raccordement (3 fils)
* 40 cables de raccordements (1 fil)
* 1 sachet raccordement
