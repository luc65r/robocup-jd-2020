#include <Ultrasonic.h>
#define DISTANCEOBSTACLE 10 //Distance à laquelle il faut contourner l'obstacle
#define DS1 1 //Distance sensor

Ultrasonic ultrasonic(6);

//Retourne la distance de l'obstacle
int obstacle() {
  int distance = analogRead(DS1);
  return distance;
}

//Contourner l'obstacle
void eviterObstacle() {
  
}

long distance() {
  return ultrasonic.MeasureInCentimeters();
}
